ARG PHP_IMAGE

FROM php:${PHP_IMAGE}

RUN set -eux; \
    apk add --no-cache --virtual .build-deps \
      g++ make autoconf yaml-dev libpng-dev libjpeg-turbo-dev libwebp-dev zlib-dev libxpm-dev libzip-dev freetype-dev libxml2-dev icu-dev oniguruma-dev libxslt-dev; \
    apk add --no-cache yaml libpng libjpeg-turbo libwebp zlib libxpm libzip freetype libxml2 icu oniguruma libxslt; \
    pecl install -fo igbinary lzf yaml \
      && docker-php-ext-enable igbinary lzf yaml; \
    pecl install --onlyreqdeps --nobuild redis \
      && cd "$(pecl config-get temp_dir)/redis" \
      && phpize \
      && ./configure --enable-redis-igbinary --enable-redis-lzf \
      && make && make install \
      && docker-php-ext-enable redis \
      && cd -; \
    docker-php-ext-configure zip; \
    docker-php-ext-configure gd --with-jpeg --with-freetype; \
    docker-php-ext-install -j$(nproc) zip gd shmop dom intl mbstring pdo_mysql xsl soap bcmath sockets pcntl mysqli; \
    apk del --purge .build-deps; \
    rm -rf /var/cache/apk/*
